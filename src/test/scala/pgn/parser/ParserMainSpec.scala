package pgn.parser

import org.specs2.mutable._

/**
 * @author János Setény
 * @since 2013.08.30.
 */
class ParserMainSpec extends Specification {

  "The parsed result for \"30. Qf4f7#\"" should {
    val list: List[Step] = PgnParser.parse("30. Qf4f7#")

    "contain 1 Step object" in {
      list must have size (1)
    }
    "be check mate" in {
      list.head.white.asInstanceOf[NormalHalfStep].moveType.get must beAnInstanceOf[CheckMatingMove]
    }
    "have no HalfStep for black" in {
      list.head.black.isEmpty must beTrue
    }
  }
}
